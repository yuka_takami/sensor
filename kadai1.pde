/*ZXセンサーを値を視覚化する*/

// TestZX01
// ZX Sensor の値（X,Z）をシリアル通信で受信し、
// Zの値で図形を画面に出力

import processing.serial.*;
Serial myPort;

int actRandomSeed = 0;
int count = 150;
int sx,sz;

/*初期設定で、サイズ、色を指定*/
void setup() {
  size(800, 800);
  colorMode(HSB, 360, 100, 100, 100);
  cursor(CROSS);
  smooth();
  myPort = new Serial(this, "/dev/ttyACM0", 9600);
  myPort.bufferUntil('\n');  //改行で終わるデータを読に指定
}

/*Arduinoから受け取ったZだけの値szを使って、図形をつくる*/
void draw() {
  background(0);
  noStroke();
  
  /*szの値が230以上だったら、szの値を230にする.
  そのszの値できれいな円になるようにmap関数で値を調整.*/
  if(sz > 230) sz = 230;
  float faderZ = (float)map(sz, 0, 230, 0.0, 1.0);
 
  randomSeed(actRandomSeed);
  float angle = radians(360/float(count));
  for (int i = 0; i  < count; i++) {
    float randomX = random(0, width);
    float randomY = random(0, height);
    float circleX = width/2 + cos(angle*i)*300;
    float circleY = height/2 + sin(angle*i)*300;

    float x = lerp(randomX, circleX, faderZ);
    float y = lerp(randomY, circleY, faderZ);

    fill(i, 50, 100);
    ellipse(x, y, 10, 10);
  }
}

/*設定したシリアルポートからデータを読み取り*/
void serialEvent(Serial myPort) {
  String str = myPort.readStringUntil('\n');  //改行まで読み込む
  str = trim(str);   //余分な空白を取り除く
   //カンマで区切られた文字列をそれぞれintに変換して、配列pos[]に格納する
  int pos[] = int(split(str, ',')); 
  if(pos.length > 1) {  // データを読み取って変換したら...
    sx = pos[0];   // pos[0]（先頭データ）をxに代入
    sz = pos[1];   // pos[1]（カンマの後のデータ）をzに代入
    println(sz);   //szの値をコンソールで確認
  }
}