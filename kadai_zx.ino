// TestZX
// ZX Sensor の動作確認（デモプログラムを変更）
// 「ファイル」!「スケッチ例」!「ZX_Sensor」!「I2C_ZX_DEMO」
/*
Hardware Connections:
 Arduino Pin ZX Sensor Board Function
 ---------------------------------------
 5V VCC Power
 GND GND Ground
 A4 DA I2C Data
 A5 CL I2C Clock
*/
#include <Wire.h>
#include <ZX_Sensor.h>
// Constants
const int ZX_ADDR = 0x10; // ZX Sensor I2C address
// Global Variables
ZX_Sensor zx_sensor = ZX_Sensor(ZX_ADDR);
uint8_t x_pos;
uint8_t z_pos;
void setup() {
 // Initialize Serial port
 Serial.begin(9600); // シリアル通信開始
 zx_sensor.init(); // センサー初期化
}
void loop() {

 // If there is position data available, read and print it
 if ( zx_sensor.positionAvailable() ) {
// x_pos = zx_sensor.readX();
 //if ( x_pos != ZX_ERROR ) {
// Serial.print("X: ");
 //Serial.print(x_pos); // X軸データ送信
// } 
    z_pos = zx_sensor.readZ();
    if ( z_pos != ZX_ERROR ) {
// Serial.print(" Z: ");
// Serial.print(",");
     Serial.println(z_pos); // Z軸データ送信
   }
   // ※↑「xの値,yの値」と「,」で区切ったデータが出力された
 }
}
